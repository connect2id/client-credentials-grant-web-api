package com.nimbusds.openid.connect.provider.spi.grants.client.webapi;


import java.util.Collections;
import java.util.List;

import junit.framework.TestCase;
import net.minidev.json.JSONObject;

import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;


public class HandlerRequestTest extends TestCase {


	public void testSerializeAndParse()
		throws Exception {

		Scope scope = new Scope("read", "write");

		ClientID clientID = new ClientID("123");

		ClientMetadata clientMetadata = new ClientMetadata();
		clientMetadata.setName("Example app");
		clientMetadata.setGrantTypes(Collections.singleton(GrantType.CLIENT_CREDENTIALS));

		HandlerRequest handlerRequest = new HandlerRequest(scope, clientID, clientMetadata);
		assertEquals(clientID, handlerRequest.getClientID());
		assertEquals(clientMetadata, handlerRequest.getClientMetadata());

		JSONObject jsonObject = handlerRequest.toJSONObject();

		System.out.println(jsonObject);
		
		assertEquals(Scope.parse("read write"), Scope.parse((List<String>) jsonObject.get("scope")));

		JSONObject clientObject = (JSONObject)jsonObject.get("client");
		assertEquals(clientID.getValue(), clientObject.get("client_id"));
		assertEquals("Example app", clientObject.get("client_name"));
		assertEquals(Collections.singletonList("client_credentials"), clientObject.get("grant_types"));
		assertEquals(3, clientObject.size());

		assertEquals(2, jsonObject.size());

		handlerRequest = HandlerRequest.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		
		assertEquals(scope, handlerRequest.getScope());
		assertEquals(clientID, handlerRequest.getClientID());
		assertEquals(clientMetadata.getName(), handlerRequest.getClientMetadata().getName());
		assertEquals(clientMetadata.toJSONObject(), handlerRequest.getClientMetadata().toJSONObject());
	}
}
