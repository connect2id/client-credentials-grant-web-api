package com.nimbusds.openid.connect.provider.spi.grants.client.webapi;


import java.util.Properties;

import junit.framework.TestCase;


public class ConfigurationTest extends TestCase {


	public void testConstructFromProperties() {

		Properties props = new Properties();
		props.setProperty("op.grantHandler.clientCredentials.webAPI.enable", "true");
		props.setProperty("op.grantHandler.clientCredentials.webAPI.url", "http://localhost:8080/client-grant-handler");
		props.setProperty("op.grantHandler.clientCredentials.webAPI.apiAccessToken", "ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6");
		props.setProperty("op.grantHandler.clientCredentials.webAPI.connectTimeout", "150");
		props.setProperty("op.grantHandler.clientCredentials.webAPI.readTimeout", "250");

		Configuration config = new Configuration(props);

		config.log();

		assertTrue(config.enable);
		assertEquals("http://localhost:8080/client-grant-handler", config.url.toString());
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.apiAccessToken.getValue());
		assertEquals(150, config.connectTimeout);
		assertEquals(250, config.readTimeout);
	}


	public void testConstructFromProperties_minimal() {

		Properties props = new Properties();
		props.setProperty("op.grantHandler.clientCredentials.webAPI.enable", "true");
		props.setProperty("op.grantHandler.clientCredentials.webAPI.url", "http://localhost:8080/client-grant-handler");
		props.setProperty("op.grantHandler.clientCredentials.webAPI.apiAccessToken", "ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6");

		Configuration config = new Configuration(props);

		config.log();

		assertTrue(config.enable);
		assertEquals("http://localhost:8080/client-grant-handler", config.url.toString());
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.apiAccessToken.getValue());
		assertEquals(0, config.connectTimeout);
		assertEquals(0, config.readTimeout);
	}


	public void testDefaults() {

		Configuration config = new Configuration(new Properties());
		assertFalse(config.enable);
		assertNull(config.url);
		assertNull(config.apiAccessToken);
		assertEquals(0, config.connectTimeout);
		assertEquals(0, config.readTimeout);
		
		config.log();
	}
}