package com.nimbusds.openid.connect.provider.spi.grants.client.webapi;


import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Optional;

import static net.jadler.Jadler.*;
import static org.junit.Assert.*;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nimbusds.common.contenttype.ContentType;
import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.grants.AccessTokenSpec;
import com.nimbusds.openid.connect.provider.spi.grants.GrantAuthorization;


/**
 * Tests the password grant API against a mock HTTP server.
 */
public class ClientCredentialsGrantWebAPITest {


	private static class HandlerRequestMatcher extends BaseMatcher<String> {

		private final ClientID clientID;

		private final ClientMetadata clientMetadata;


		public HandlerRequestMatcher(final ClientID clientID,
					     final ClientMetadata clientMetadata) {

			this.clientID = clientID;
			this.clientMetadata = clientMetadata;
		}


		@Override
		public boolean matches(Object o) {

			HandlerRequest handlerRequest;
			try {
				handlerRequest = HandlerRequest.parse(JSONObjectUtils.parse(o.toString()));
			} catch (Exception e) {
				return false;
			}

			if (! clientID.equals(handlerRequest.getClientID())) {
				return false;
			}
			
			if (! clientMetadata.toJSONObject().equals(handlerRequest.getClientMetadata().toJSONObject())) {
				return false;
			}
				
			return true;
		}


		@Override
		public void describeTo(Description description) {

		}
	}


	@Before
	public void setUp() {
		initJadler();
	}

	@After
	public void tearDown() {
		
		System.clearProperty("op.grantHandler.clientCredentials.webAPI.enable");
		System.clearProperty("op.grantHandler.clientCredentials.webAPI.url");
		System.clearProperty("op.grantHandler.clientCredentials.webAPI.apiAccessToken");
		System.clearProperty("op.grantHandler.clientCredentials.webAPI.connectTimeout");
		System.clearProperty("op.grantHandler.clientCredentials.webAPI.readTimeout");
		
		closeJadler();
	}


	@Test
	public void testGoodRequest()
		throws Exception {

		Scope scope = new Scope("read", "write");
		
		ClientID clientID = new ClientID("123");
		ClientMetadata clientMetadata = new ClientMetadata();
		clientMetadata.setGrantTypes(Collections.singleton(GrantType.CLIENT_CREDENTIALS));
		AccessTokenSpec accessTokenSpec = new AccessTokenSpec(0L, null, TokenEncoding.SELF_CONTAINED, Optional.of(false));

		GrantAuthorization authzIn = new GrantAuthorization(
			new Scope("read"),
			accessTokenSpec,
			null // no optional authorisation data
		);

		onRequest()
			.havingMethodEqualTo("POST")
			.havingBody(new HandlerRequestMatcher(clientID, clientMetadata))
		.respond()
			.withStatus(200)
			.withBody(authzIn.toJSONObject().toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType(ContentType.APPLICATION_JSON.toString());

		InitContext initCtx = new MockInitContext(false, new URL("http://localhost:" + port()));

		ClientCredentialsGrantWebAPI api = new ClientCredentialsGrantWebAPI();
		api.init(initCtx);
		assertEquals(GrantType.CLIENT_CREDENTIALS, api.getGrantType());

		GrantAuthorization authzOut = api.processGrant(scope, clientID, clientMetadata);
		assertEquals(authzIn.getScope(), authzOut.getScope());
		assertEquals(0L, authzOut.getAccessTokenSpec().getLifetime());
		assertNull(authzOut.getAccessTokenSpec().getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, authzOut.getAccessTokenSpec().getEncoding());
		assertFalse(authzOut.getAccessTokenSpec().getEncryptSelfContained().get());
	}


	@Test
	public void testGoodRequest_systemPropertiesConfig()
		throws Exception {
		
		Scope scope = new Scope("read", "write");
		
		ClientID clientID = new ClientID("123");
		ClientMetadata clientMetadata = new ClientMetadata();
		clientMetadata.setGrantTypes(Collections.singleton(GrantType.CLIENT_CREDENTIALS));
		AccessTokenSpec accessTokenSpec = new AccessTokenSpec(0L, null, TokenEncoding.SELF_CONTAINED, Optional.of(false));
		
		GrantAuthorization authzIn = new GrantAuthorization(
			new Scope("read"),
			accessTokenSpec,
			null // no optional authorisation data
		);

		onRequest()
			.havingMethodEqualTo("POST")
			.havingBody(new HandlerRequestMatcher(clientID, clientMetadata))
		.respond()
			.withStatus(200)
			.withBody(authzIn.toJSONObject().toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType(ContentType.APPLICATION_JSON.toString());

		URL endpoint = new URL("http://localhost:" + port());
		
		InitContext initCtx = new MockInitContext(true, endpoint);
		
		System.setProperty("op.grantHandler.clientCredentials.webAPI.enable", "true");
		System.setProperty("op.grantHandler.clientCredentials.webAPI.url", endpoint.toString());
		System.setProperty("op.grantHandler.clientCredentials.webAPI.apiAccessToken", "ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6");

		ClientCredentialsGrantWebAPI api = new ClientCredentialsGrantWebAPI();
		api.init(initCtx);
		assertEquals(GrantType.CLIENT_CREDENTIALS, api.getGrantType());
		
		GrantAuthorization authzOut = api.processGrant(scope, clientID, clientMetadata);
		assertEquals(authzIn.getScope(), authzOut.getScope());
		assertEquals(0L, authzOut.getAccessTokenSpec().getLifetime());
		assertNull(authzOut.getAccessTokenSpec().getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, authzOut.getAccessTokenSpec().getEncoding());
		assertFalse(authzOut.getAccessTokenSpec().getEncryptSelfContained().get());
	}


	@Test
	public void testProcessHTTP500Response() {

		HTTPResponse httpResponse = new HTTPResponse(HTTPResponse.SC_SERVER_ERROR);

		ErrorObject errorObject = ClientCredentialsGrantWebAPI.processNon200Response(httpResponse);
		
		assertEquals(OAuth2Error.SERVER_ERROR, errorObject);
		assertEquals(500, errorObject.getHTTPStatusCode());
	}


	@Test
	public void testProcessHTTP400InvalidScope() {

		HTTPResponse httpResponse = new HTTPResponse(HTTPResponse.SC_BAD_REQUEST);
		httpResponse.setEntityContentType(ContentType.APPLICATION_JSON);
		httpResponse.setContent(OAuth2Error.INVALID_SCOPE.toJSONObject().toJSONString());

		ErrorObject errorObject = ClientCredentialsGrantWebAPI.processNon200Response(httpResponse);
		
		assertEquals(OAuth2Error.INVALID_SCOPE, errorObject);
		assertEquals(400, errorObject.getHTTPStatusCode());
	}


	@Test
	public void testOverrideEnable()
		throws Exception {

		System.setProperty("op.grantHandler.clientCredentials.webAPI.enable", "false");

		InitContext initCtx = new MockInitContext(false, new URL("http://localhost:" + port()));

		ClientCredentialsGrantWebAPI api = new ClientCredentialsGrantWebAPI();
		api.init(initCtx);

		assertFalse(api.isEnabled());
	}


	@Test
	public void testOverrideToken()
		throws Exception {

		BearerAccessToken token = new BearerAccessToken(64);

		System.setProperty("op.grantHandler.clientCredentials.webAPI.apiAccessToken", token.getValue());

		InitContext initCtx = new MockInitContext(false, new URL("http://localhost:" + port()));

		ClientCredentialsGrantWebAPI api = new ClientCredentialsGrantWebAPI();
		api.init(initCtx);

		assertEquals(token.getValue(), api.getConfiguration().apiAccessToken.getValue());
	}
}
