/**
 * Web API for handling client credential grants.
 */
package com.nimbusds.openid.connect.provider.spi.grants.client.webapi;