package com.nimbusds.openid.connect.provider.spi.grants.client.webapi;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nimbusds.common.contenttype.ContentType;
import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.grants.ClientCredentialsGrantHandler;
import com.nimbusds.openid.connect.provider.spi.grants.GrantAuthorization;

/**
 * Client credentials grant web API.
 */
public class ClientCredentialsGrantWebAPI implements ClientCredentialsGrantHandler {


	/**
	 * The configuration file path.
	 */
	public static final String CONFIG_FILE_PATH = "/WEB-INF/clientGrantHandlerWebAPI.properties";


	/**
	 * The configuration.
	 */
	private Configuration config;


	/**
	 * The main logger.
	 */
	private static final Logger mainLog = LogManager.getLogger("MAIN");


	/**
	 * The token endpoint logger.
	 */
	private static final Logger tokenEndpointLog = LogManager.getLogger("TOKEN");


	/**
	 * Loads the configuration.
	 *
	 * @param initContext The initialisation context. Must not be
	 *                    {@code null}.
	 *
	 * @return The configuration.
	 *
	 * @throws Exception If loading failed.
	 */
	private static Configuration loadConfiguration(final InitContext initContext)
		throws Exception {

		Properties props = new Properties();
		
		InputStream inputStream = initContext.getResourceAsStream(CONFIG_FILE_PATH);
		
		if (inputStream != null) {
			props.load(inputStream);
		}

		return new Configuration(props);
	}


	@Override
	public void init(final InitContext initContext)
		throws Exception {

		mainLog.info("[CGH 0005] Initializing client credentials grant handler...");
		config = loadConfiguration(initContext);
		config.log();
	}


	/**
	 * Returns the configuration.
	 *
	 * @return The configuration.
	 */
	public Configuration getConfiguration() {

		return config;
	}


	@Override
	public boolean isEnabled() {

		return config.enable;
	}


	@Override
	public GrantType getGrantType() {

		return GrantType.CLIENT_CREDENTIALS;
	}


	@Override
	public GrantAuthorization processGrant(final Scope scope,
					       final ClientID clientID,
					       final ClientMetadata clientMetadata)
		throws GeneralException {

		if (! config.enable) {
			throw new GeneralException("Grant handler disabled", OAuth2Error.UNSUPPORTED_GRANT_TYPE);
		}

		tokenEndpointLog.debug("[CGH 0006] Client credentials grant handler: Received request with scope={}", scope);

		HandlerRequest request = new HandlerRequest( scope, clientID, clientMetadata);

		HTTPRequest httpRequest = new HTTPRequest(HTTPRequest.Method.POST, config.url);
		httpRequest.setAuthorization(config.apiAccessToken.toAuthorizationHeader());
		httpRequest.setEntityContentType(ContentType.APPLICATION_JSON);
		httpRequest.setQuery(request.toJSONObject().toJSONString());
		httpRequest.setConnectTimeout(config.connectTimeout);
		httpRequest.setReadTimeout(config.readTimeout);

		tokenEndpointLog.debug("[CGH 0007] Client credentials grant handler: Making HTTP post request to {}", httpRequest.getURL());

		HTTPResponse httpResponse;

		try {
			httpResponse = httpRequest.send();

		} catch (IOException e) {
			// Log and return HTTP 500 server_error back to OAuth 2.0 client
			tokenEndpointLog.error("[CGH 0008] Client credentials grant handler: HTTP exception: " + e.getMessage(), e);
			throw new GeneralException("Server error", OAuth2Error.SERVER_ERROR);
		}

		if (! httpResponse.indicatesSuccess()) {
			ErrorObject errorObject = processNon200Response(httpResponse);
			throw new GeneralException(errorObject.getCode(), errorObject);
		}

		tokenEndpointLog.debug("[CGH 0009] Client credentials grant handler: Received authorization response: {}", httpResponse.getContent());

		try {
			return GrantAuthorization.parse(httpResponse.getContentAsJSONObject());

		} catch (Exception e) {
			// Log and pass HTTP 500 server_error back to OAuth 2.0 client
			tokenEndpointLog.error("[CGH 0010] Client credentials grant handler: Invalid authorization response: {}", e.getMessage(), e);
			throw new GeneralException("Server error", OAuth2Error.SERVER_ERROR);
		}
	}


	/**
	 * Processes a non-200 HTTP response to produce an OAuth 2.0 error
	 * object to return to the OAuth 2.0 client.
	 *
	 * @param httpResponse The HTTP response, with a status code other than
	 *                     200. Must not be {@code null}.
	 *
	 * @return The resulting error object.
	 */
	public static ErrorObject processNon200Response(final HTTPResponse httpResponse) {

		// Check for non 400
		if (httpResponse.getStatusCode() != HTTPResponse.SC_BAD_REQUEST) {
			// Unexpected HTTP status code
			tokenEndpointLog.error("[CGH 0011] Client credentials grant handler: Unexpected HTTP response: {}", httpResponse.getStatusCode());
			return OAuth2Error.SERVER_ERROR;
		}

		// Check error code
		ErrorObject errorObject = ErrorObject.parse(httpResponse);

		if (errorObject.getCode() == null ||
		    ! errorObject.equals(OAuth2Error.INVALID_GRANT) &&
		    ! errorObject.equals(OAuth2Error.INVALID_SCOPE)) {

			// Missing or unexpected error code
			tokenEndpointLog.error("[CGH 0012] Client credentials grant handler: Missing or unexpected error code: {}", errorObject.getCode());
			return OAuth2Error.SERVER_ERROR;
		}

		// Return parsed invalid_grant or invalid_scope
		tokenEndpointLog.info("[CGH 0013] Client credentials grant handler: Token request denied: {}", errorObject.getCode());
		return errorObject;
	}


	@Override
	public void shutdown() {

		// Nothing to do

		mainLog.info("[CGH 0014] Shutting down client credentials grant handler...");
	}
}
