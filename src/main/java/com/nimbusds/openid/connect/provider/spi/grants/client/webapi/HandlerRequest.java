package com.nimbusds.openid.connect.provider.spi.grants.client.webapi;


import net.minidev.json.JSONObject;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;


/**
 * Encapsulates a request to the client credentials grant web API.
 */
public final class HandlerRequest {


	/**
	 * The requested scope.
	 */
	private final Scope scope;


	/**
	 * The client identifier.
	 */
	private final ClientID clientID;


	/**
	 * The OAuth 2.0 client metadata.
	 */
	private final ClientMetadata clientMetadata;


	/**
	 * Creates a new handler request.
	 *
	 * @param scope          The requested scope, {@code null} if not
	 *                       specified.
	 * @param clientID       The client identifier. Must not be
	 *                       {@code null}.
	 * @param clientMetadata The OAuth 2.0 client metadata. Must not be
	 *                       {@code null}.
	 */
	public HandlerRequest(final Scope scope,
			      final ClientID clientID,
			      final ClientMetadata clientMetadata) {

		this.scope = scope;

		if (clientID == null) {
			throw new IllegalArgumentException("The client ID must not be null");
		}
		this.clientID = clientID;

		if (clientMetadata == null) {
			throw new IllegalArgumentException("The client metadata must not be null");
		}

		this.clientMetadata = clientMetadata;
	}


	/**
	 * Returns the requested scope.
	 *
	 * @return The scope, {@code null} if not specified.
	 */
	public Scope getScope() {

		return scope;
	}


	/**
	 * Returns the client identifier.
	 *
	 * @return The client identifier.
	 */
	public ClientID getClientID() {

		return clientID;
	}


	/**
	 * Returns the OAuth 2.0 client metadata.
	 *
	 * @return The client metadata.
	 */
	public ClientMetadata getClientMetadata() {

		return clientMetadata;
	}


	/**
	 * Returns a JSON object representation of this request.
	 *
	 * <p>Example JSON object:
	 *
	 * <pre>
	 * {
	 *   "scope"    : [ "read" , "write", "admin ],
	 *   "client"   : { "client_id"   : "123",
	 *                  "client_name" : "Example app",
	 *                  "grant_types" : [ "client_credentials" ],
	 *                  "scope"       : "read write admin" }
	 * }
	 * </pre>
	 *
	 * @return The JSON object.
	 */
	public JSONObject toJSONObject() {

		JSONObject o = new JSONObject();

		if (scope != null) {
			o.put("scope", scope.toStringList());
		}

		JSONObject clientJSONObject = clientMetadata.toJSONObject();
		clientJSONObject.put("client_id", clientID.getValue());
		o.put("client", clientJSONObject);

		return o;
	}


	/**
	 * Parses a handler request from the specified JSON object.
	 *
	 * @param jsonObject The JSON object to parse. Must not be
	 *                   {@code null}.
	 *
	 * @return The handler request.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static HandlerRequest parse(final JSONObject jsonObject)
		throws ParseException {

		Scope scope = null;

		if (jsonObject.containsKey("scope")) {
			scope = new Scope(JSONObjectUtils.getStringArray(jsonObject, "scope"));
		}

		JSONObject clientObject = JSONObjectUtils.getJSONObject(jsonObject, "client");

		ClientID clientID = new ClientID(JSONObjectUtils.getString(clientObject, "client_id"));

		JSONObject clientMetadataObject = new JSONObject(clientObject);
		clientMetadataObject.remove("client_id");
		ClientMetadata clientMetadata = ClientMetadata.parse(clientMetadataObject);

		return new HandlerRequest(scope, clientID, clientMetadata);
	}
}
