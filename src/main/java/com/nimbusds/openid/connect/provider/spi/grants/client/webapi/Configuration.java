package com.nimbusds.openid.connect.provider.spi.grants.client.webapi;


import java.net.URL;
import java.util.Properties;

import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.nimbusds.common.config.ConfigurationException;
import com.nimbusds.common.config.LoggableConfiguration;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;


/**
 * Client credentials grant handler configuration. It is typically derived from
 * a Java key / value properties file. The configuration is stored as public
 * fields which become immutable (final) after their initialisation.
 *
 * <p>Example configuration properties:
 *
 * <pre>
 * op.grantHandler.clientCredentials.webAPI.enable = true
 * op.grantHandler.clientCredentials.webAPI.url = http://localhost:8080/client-grant-handler
 * op.grantHandler.clientCredentials.webAPI.apiAccessToken = ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6
 * op.grantHandler.clientCredentials.webAPI.connectTimeout = 150
 * op.grantHandler.clientCredentials.webAPI.readTimeout = 250
 * </pre>
 */
public final class Configuration implements LoggableConfiguration {


	/**
	 * The properties prefix.
	 */
	public static final String PREFIX = "op.grantHandler.clientCredentials.webAPI.";


	/**
	 * Enables / disables the client credentials grant handler.
	 */
	public final boolean enable;


	/**
	 * The endpoint URL of the web API.
	 */
	public final URL url;


	/**
	 * Access token of type Bearer for the web API.
	 */
	public final BearerAccessToken apiAccessToken;


	/**
	 * The HTTP connect timeout, in milliseconds. Zero implies none.
	 */
	public final int connectTimeout;


	/**
	 * The HTTP response read timeout, in milliseconds. Zero implies none.
	 */
	public final int readTimeout;


	/**
	 * Creates a new client credentials grant handler configuration from
	 * the specified properties.
	 *
	 * @param props The properties. Must not be {@code null}.
	 *
	 * @throws ConfigurationException On a missing or invalid property.
	 */
	public Configuration(final Properties props)
		throws ConfigurationException {

		PropertyRetriever pr = new PropertyRetriever(props, true);

		try {
			enable = pr.getOptBoolean(PREFIX + "enable", false);
			
			if (! enable) {
				url = null;
				apiAccessToken = null;
				connectTimeout = 0;
				readTimeout = 0;
				return;
			}
			
			url = pr.getURL(PREFIX + "url");
			apiAccessToken = new BearerAccessToken(pr.getString(PREFIX + "apiAccessToken"));
			connectTimeout = pr.getOptInt(PREFIX + "connectTimeout", 0);
			readTimeout = pr.getOptInt(PREFIX + "readTimeout", 0);
		} catch (PropertyParseException e) {
			throw new ConfigurationException(e.getMessage() + ": Property: " + e.getPropertyKey());
		}
	}


	/**
	 * Logs the configuration details at INFO level. Properties that may
	 * adversely affect security are logged at WARN level.
	 */
	@Override
	public void log() {

		Logger log = LogManager.getLogger("MAIN");

		log.info("[CGH 0000] Client credentials grant handler web API enabled: {}", enable);
		
		if (! enable) {
			return;
		}
		
		log.info("[CGH 0001] Client credentials grant handler web API URL: {}", url);

		if (connectTimeout > 0) {
			log.info("[CGH 0002] Client credentials grant handler web API HTTP connect timeout: {} ms", connectTimeout);
		} else {
			log.info("[CGH 0002] Client credentials grant handler web API HTTP connect timeout: none");
		}

		if (readTimeout > 0) {
			log.info("[CGH 0003] Client credentials grant handler web API HTTP read timeout: {} ms", readTimeout);
		} else {
			log.info("[CGH 0003] Client credentials grant handler web API HTTP read timeout: none");
		}
	}
}
